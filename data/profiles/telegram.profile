<service xmlns="http://telepathy.freedesktop.org/wiki/service-profile-v1"
         id="telegram"
         type="IM"
         manager="morse"
         protocol="telegram"
         icon="im-telegram">
  <name>Telegram Network</name>
</service>
