[Desktop Entry]
Encoding=UTF-8
Comment=Account configuration user interface for Telegram Network using the Morse connection manager.
Type=Service
ServiceTypes=KTpAccountsKCM/AccountUiPlugin

X-KDE-Library=ktpaccountskcm_plugin_morse
X-KDE-PluginInfo-Author=Alexandr Akulich
X-KDE-PluginInfo-Email=akulichalexander@gmail.com
X-KDE-PluginInfo-Name=morse
X-KDE-PluginInfo-Version=@KTP_ACCOUNTS_KCM_VERSION@
X-KDE-PluginInfo-Website=http://techbase.kde.org/Projects/Telepathy
X-KDE-PluginInfo-License=GPL
X-KDE-PluginInfo-EnabledByDefault=true
